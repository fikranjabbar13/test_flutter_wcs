import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller/film_ctrl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: HomePage());
  }
}

class HomePage extends StatelessWidget {
  // ignore: non_constant_identifier_names
  final DataFilmController CDataFilm = Get.put(DataFilmController());
  final listBgColor = const [
    Color(0xFFE7F8FF),
    Color.fromARGB(255, 211, 211, 211),
    Color.fromARGB(255, 67, 0, 39)
    
  ];
  final textColor = const [
    Color.fromARGB(255, 255, 255, 255),
    Color.fromARGB(255, 167, 74, 179),
    Color(0xFFD87C96)
  ];

  HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CDataFilm.getDataFilm(); //getFIrstData
    return Scaffold(
      appBar: AppBar(
        title: const Text('Test Flutter WCS'),
        backgroundColor:Color.fromARGB(255, 55, 5, 32) ,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 8, right: 8),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            const SizedBox(height: 10),
           const TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Search Film',
              ),
            ),
            const SizedBox(height: 12),
            Expanded(
              child: Obx(() => CDataFilm.MFilm.value.result != null &&
                      CDataFilm.MFilm.value.result!.isNotEmpty
                  ? GridView.builder(
                    gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 100,
                childAspectRatio: 4 / 3,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10),
     
                      // shrinkWrap: true,
                      // physics: const NeverScrollableScrollPhysics(),
                      itemCount: CDataFilm.MFilm.value.result!.length,

                      itemBuilder: ItemFilm,
                    )
                  : const Center(
                      child: Text('Loading ..'),
                    )),
            ),
             Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                    style: TextButton.styleFrom(
                        backgroundColor: const Color(0xffF18265)),
                    onPressed: () {},
                    child:  Text(
                      "<<", style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: textColor[0]),
                    ),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                        backgroundColor: const Color(0xffF18265)),
                    onPressed: () {},
                    child:  Text(
                      "<", style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: textColor[0]),
                    ),
                  ),
                  Text(
                      "1", style: TextStyle(
                        backgroundColor: textColor[1],
                          fontWeight: FontWeight.bold,
                          color: textColor[0]),
                    ),
                  Text(
                      "2", style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: textColor[1]),
                    ),
                
                   TextButton(
                    style: TextButton.styleFrom(
                        backgroundColor: const Color(0xffF18265)),
                    onPressed: () {},
                    child:  Text(
                      ">", style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: textColor[0]),
                    ),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                        backgroundColor: const Color(0xffF18265)),
                    onPressed: () {},
                    child:  Text(
                      ">>", style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: textColor[0]),
                    ),
                  ),
            ],)
          ],
        ),
      ),
    );
  }

  // ignore: non_constant_identifier_names
  Widget ItemFilm(context, index) {
    final dataFilm = CDataFilm.MFilm.value.result![index];
    return Container(
      margin: const EdgeInsets.all(2),
      decoration: BoxDecoration(
          // color: listBgColor[index % 3],
          borderRadius: const BorderRadius.all(Radius.circular(10.0)),
          gradient: LinearGradient(
              colors: [listBgColor[2], listBgColor[2]],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              tileMode: TileMode.repeated),
          boxShadow: [
            BoxShadow(
              color: listBgColor[1],
              spreadRadius: 3,
              blurRadius: 3,
              offset: const Offset(0, 1), // changes position of shadow
            ),
          ]),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
           SizedBox(
                    // height: 20,
                    child: Text(
                      "${dataFilm.title}",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: textColor[0]),
                    )),
            
          ],
        ),
      ),
    );
  }
}
